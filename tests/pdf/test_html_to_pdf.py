from lupin_reporting.pdf.html_to_pdf import HtmlToPdf
from os import path


def test_html_to_pdf_file():
    file = r'tests/html/input/index.html'
    output_file = r'tests/html/output/out.pdf'
    HtmlToPdf(file).convert(output_file)
    assert path.exists(output_file)
