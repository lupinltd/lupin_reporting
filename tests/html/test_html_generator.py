import datetime as dt
from lupin_reporting.html.html_generator import HtmlGenerator
from lupin_reporting.html.enums import Reports
from lupin_reporting.plotting.insurance_plotter import InsurancePlotter
from lupin_reporting.plotting.need_analysis_plotter import NeedAnalysisPlotter


def test_html_generator_insurance_success():
    output_file = 'output/output.html'
    plotter = InsurancePlotter()
    needs_analysis = NeedAnalysisPlotter()
    #### INPUTS
    AGE = 32
    MORTALITY_AGE = 95
    PERCENT_INCOME_NEEDED = [0.50] * (MORTALITY_AGE - AGE)
    CURRENT_SALARY = 120000
    RETIREMENT_AGE = 60


    generator = HtmlGenerator(Reports.INSURANCE_REPORT)
    content_kwargs = {
        'title': 'Cordoba Estate Insurance Report',
        'report_description': 'Generated Insurance Report by Lupin Software Co.',
        'subtitle': 'Prepared by Joel Robertson',
        'publish_date': dt.datetime.now().strftime('%Y-%M-%d'),
        'report_author': 'Joel Robertson',
        'needs_analysis_plot': needs_analysis.as_html(AGE, MORTALITY_AGE, PERCENT_INCOME_NEEDED, CURRENT_SALARY,
                                                      RETIREMENT_AGE),
        'insurance_policy_fit_plot': plotter.insurance_policy_fit(),
        'insurance_policy_fit_comparison_plot': plotter.insurance_policy_comparison()
    }


    generator.generate(content_kwargs, output_location=output_file)


def test_html_generator_pick_em_success():
    puts_html ='<table></table>'
    generator = HtmlGenerator(Reports.PICKEM_REPORT)
    content_kwargs = {
        'week_name': '',
        'puts_table': puts_html
    }
    generator.generate(content_kwargs, output_location='output/output.html')


#test_html_generator_insurance_success()