import pdfkit


class HtmlToPdf():

    DEFAULT_OPTIONS = {
        "enable-local-file-access": None
    }

    def __init__(self,  source_file=None, options=None):
        self._source_file = source_file
        self._options = options if options else self.DEFAULT_OPTIONS

    def convert(self, output_filename):
        pdfkit.from_url(self._source_file, output_filename, options=self._options)