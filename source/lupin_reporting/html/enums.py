import enum


class Reports(enum.Enum):
    INSURANCE_REPORT = 'insurance_report.html'
    PICKEM_REPORT = 'pickem_report.html'
