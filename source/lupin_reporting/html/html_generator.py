from distutils.dir_util import copy_tree
import os
from jinja2 import Environment, FileSystemLoader
from lupin_reporting.html.constants import DEFAULT_TEMPLATES_DIRECTORY, DEFAULT_ASSETS_DIRECTORY, ASSETS_DIRECTORY_NAME


class HtmlGenerator:

    TEMPLATES_DIRECTORY = 'template_directory'
    ASSETS_DIRECTORY = 'assets_directory'

    def __init__(self, report_enum, options=None):
        self._report_enum = report_enum
        self._options = options if options else dict()

    def generate(self, content_kwargs, output_location=None):
        template_directory = self._get_template_directory_name()
        env = Environment(loader=FileSystemLoader(template_directory))
        template = env.get_template(self._report_enum.value)
        rendered_template = template.render(**content_kwargs)
        if output_location:
            output_root = os.path.join(*os.path.split(output_location)[:-1])
            copy_tree(self._get_assets_directory_name(), os.path.join(output_root, ASSETS_DIRECTORY_NAME))
            with open(output_location, "w") as output_file:
                output_file.write(rendered_template)
        return rendered_template

    def _get_template_directory_name(self):
        return self._options[self.TEMPLATES_DIRECTORY] \
            if self.TEMPLATES_DIRECTORY in self._options else \
            os.path.join(HtmlGenerator._get_absolute_root(), DEFAULT_TEMPLATES_DIRECTORY)

    def _get_assets_directory_name(self):
        return self._options[self.ASSETS_DIRECTORY] \
            if self.ASSETS_DIRECTORY in self._options else \
            os.path.join(HtmlGenerator._get_absolute_root(), DEFAULT_ASSETS_DIRECTORY)

    @staticmethod
    def _get_absolute_root():
        abspath = os.path.abspath(__file__)
        return os.path.dirname(abspath)
