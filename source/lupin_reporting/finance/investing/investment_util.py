import datetime as dt
import dateutil.relativedelta as rd
from pretty_html_table import build_table
import smtplib, ssl
from os.path import basename
from email.mime.application import MIMEApplication
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from lupin_reporting.finance.investing.constants import SENDER_EMAIL, SENDER_PASSWORD, GMAIL_SERVER, SQLALCHEMY_DATABASE_URI
from sqlalchemy import create_engine
import json


class InvestmentUtil():

    DT_FORMAT_STR = '%Y-%m-%d'

    @staticmethod
    def get_next_friday_str():
        return InvestmentUtil.format_to_date_string(InvestmentUtil.get_next_friday_dt())

    @staticmethod
    def format_to_date_string(format_dt):
        return format_dt.strftime(InvestmentUtil.DT_FORMAT_STR)

    @staticmethod
    def get_next_friday_dt():
        today = dt.date.today()
        delta = rd.relativedelta(days=1, weekday=rd.FR)
        next_friday = today + delta
        return next_friday

    @staticmethod
    def df_to_html(df):
        return build_table(df, 'blue_light')

    @staticmethod
    def send_df_as_gmail(df, subject, receipients):
        InvestmentUtil.send_html_as_gmail(InvestmentUtil.df_to_html(df), subject, receipients)

    @staticmethod
    def send_html_as_gmail(html, subject, receipients, attachment_file=None):
        receiver_email = ', '.join(receipients)
        message = MIMEMultipart("alternative")
        message["Subject"] = subject
        message["From"] = SENDER_EMAIL
        message["To"] = receiver_email
        if attachment_file is not None:
            message = InvestmentUtil._attach_file_to_message(message, attachment_file)
        message.attach(MIMEText(html, "html"))

        # Create secure connection with server and send email
        context = ssl.create_default_context()
        with smtplib.SMTP_SSL(GMAIL_SERVER, 465, context=context) as server:
            server.login(SENDER_EMAIL, SENDER_PASSWORD)
            server.sendmail(
                SENDER_EMAIL, receipients, message.as_string()
            )

    @staticmethod
    def df_to_json(df):
        return json.loads(df.to_json(orient="records"))

    @staticmethod
    def _attach_file_to_message(message, attachment_file):
        with open(attachment_file, "rb") as fil:
            part = MIMEApplication(
                fil.read(),
                Name=basename(attachment_file)
            )
        # After the file is closed
        part['Content-Disposition'] = 'attachment; filename="%s"' % basename(attachment_file)
        message.attach(part)
        return message

    @staticmethod
    def get_database_engine():
        return create_engine(SQLALCHEMY_DATABASE_URI, echo=False)
