import yfinance as yf
import numpy as np
import logging
from lupin_reporting.finance.investing.technical_analysis import TechnicalAnalysis


class OptionsFetcher():

    DEFAULT_ORDER_BY = ['percent_premium']
    DEFAULT_DATA_COUNT = 8
    PERIOD_DAYS_TO_FETCH = 30

    @staticmethod
    def fetch_covered_wheel_data(ticker_str, option_chain, order_by=DEFAULT_ORDER_BY, number_of_points=DEFAULT_DATA_COUNT,
                               ignore_itm=True, ascending=False):
        logging.info("Fetching count for ticker: " + ticker_str)
        ticker = yf.Ticker(ticker_str)
        try:
            opt_chain = ticker.option_chain(option_chain)
        except ValueError:
            logging.warning("No option chain found for {option_chain} for ticker {ticker}".format(
                option_chain=option_chain, ticker=ticker_str))
            return None
        data_history = ticker.history(period='{}d'.format(OptionsFetcher.PERIOD_DAYS_TO_FETCH))
        rsi_14_df = TechnicalAnalysis.get_rsi(data_history['Close'], 14).tail(1)
        rsi_10_df = TechnicalAnalysis.get_rsi(data_history['Close'], 10).tail(1)
        previous_close_price = data_history.tail(1).Close.iloc[0] # get last row close

        # TODO: remove duplicate logic (add common to output table)

        # puts
        puts = opt_chain.puts
        puts['percent_premium'] = puts.lastPrice / puts.strike
        puts['premium_return'] = puts.lastPrice * 100.0
        puts['investment_cost'] = puts.strike * 100.0
        puts['ticker'] = ticker_str
        puts['option_chain'] = option_chain
        puts['latest_rsi_14'] = rsi_14_df.rsi.iloc[0]
        puts['latest_rsi_14_indicator'] = rsi_14_df.rsi_indicator.iloc[0]
        puts['latest_rsi_10'] = rsi_10_df.rsi.iloc[0]
        puts['latest_rsi_10_indicator'] = rsi_10_df.rsi_indicator.iloc[0]
        puts['percent_from_the_money'] = 100.0*(puts.strike - previous_close_price) / previous_close_price

        # calls
        calls = opt_chain.calls

        calls['percent_premium'] = calls.lastPrice / calls.strike
        calls['premium_return'] = calls.lastPrice * 100.0
        calls['investment_cost'] = calls.strike * 100.0
        calls['ticker'] = ticker_str
        calls['option_chain'] = option_chain
        calls['latest_rsi_14'] = rsi_14_df.rsi.iloc[0]
        calls['latest_rsi_14_indicator'] = rsi_14_df.rsi_indicator.iloc[0]
        calls['latest_rsi_10'] = rsi_10_df.rsi.iloc[0]
        calls['latest_rsi_10_indicator'] = rsi_10_df.rsi_indicator.iloc[0]
        calls['percent_from_the_money'] = 100.0 * (calls.strike - previous_close_price) / previous_close_price

        # compute puts/calls ratio
        calls = calls.set_index('strike')
        puts = puts.set_index('strike')
        put_calls_ratio_df = puts[['openInterest']].join(calls[['openInterest']].openInterest, lsuffix='_puts', rsuffix='_calls')
        put_calls_ratio_df['put_call_ratio'] = put_calls_ratio_df['openInterest_puts'] / put_calls_ratio_df['openInterest_calls']
        put_calls_ratio_df = put_calls_ratio_df.replace([np.inf, -np.inf], np.nan)
        puts['putCallRatio'] = put_calls_ratio_df['put_call_ratio']
        calls['putCallRatio'] = put_calls_ratio_df['put_call_ratio']

        if ignore_itm:
            puts = (puts[puts['inTheMoney'] == False])
        puts = puts.sort_values(by=order_by, ascending=ascending).head(number_of_points)

        if ignore_itm:
            calls = (calls[calls['inTheMoney'] == False])
        calls = calls.sort_values(by=order_by, ascending=ascending).head(number_of_points)

        return puts, calls


if __name__ == '__main__':
    ticker = yf.Ticker('AAPL')
    df = ticker.history(period='30d')
    data = df['Close']
