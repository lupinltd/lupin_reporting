from lupin_reporting.finance.investing.options_fetcher import OptionsFetcher
from lupin_reporting.finance.investing.investment_util import InvestmentUtil
from lupin_reporting.html.html_generator import HtmlGenerator
from lupin_reporting.html.enums import Reports
import pandas as pd
import logging
import dateutil.relativedelta as relativedelta


class WheelPickem():

    DEFAULT_STOCK_LIST = ["AAPL" , "FB", "GOOG", "TSLA", "MSFT", "QQQ", "AMD", "NVDA", "INTC", "CTXS", "UBER", "VMW", "ORCL",
              "PLTR", "CRSR", "GME", "TLRY", "APHA", "BB", "ICLN",
    "ADBE", "CSCO", "LRCX", "QCOM", "VZ", "T", "AMT", "SWKS", "V", "MA", "C", "GS",
    "JPM", "BAC", "BX", "PNC", "PYPL", "AMZN", "BABA", "BBY", "WMT", "NOC", "BA", "LMT", "EA",
    "ATVI", "ZNGA", "DIS", "NFLX", "AMGN", "BIIB", "REGN", "GILD", "MRK", "ABBV",
    "PFE", "VRTX", "JNJ", "ABT", "BDX", "DOW", "DD", "MMM", "HD", "LOW", "MCD", "CMG", "BYND",
    "KO", "STZ", "BUD", "TAP", "GRUB", "YELP", "MKC", "SBUX", "UAL", "LUV", "DAL", "CCL", "RCL",
    "NCLH", "JETS", "SNAP", "TQQQ", "SPY", "XLF", "NIO", "BMY", "XOM"]
    NUMBER_OF_WEEKS_TO_GATHER = 20
    OUTPUT_FILE = 'output/output.html'
    MESSAGE = 'Please See Attached Report'
    RECIPIENTS = [
        'cordobamatt@gmail.com',
        'j.robertson@latitude-west.ca',
        'dnadeau16@gmail.com'
    ]

    @staticmethod
    def get_as_json(option_chain=None, stock_list=None):
        option_chain, puts_df, calls_df = WheelPickem._get_result(option_chain=option_chain, stock_list=stock_list)
        return {
            'option_chain': option_chain,
            'puts': InvestmentUtil.df_to_json(puts_df),
            'calls': InvestmentUtil.df_to_json(calls_df)
        }

    @staticmethod
    def get_as_df(option_chain=None, stock_list=None):
        return WheelPickem._get_result(option_chain=option_chain, stock_list=stock_list)

    @staticmethod
    def get_pickem_result_from_database(option_type, option_chain):
        engine = InvestmentUtil.get_database_engine()
        df = None
        with engine.connect() as conn:
            query = "SELECT * FROM {option_type} WHERE option_chain = '{option_chain}'".format(option_type=option_type,
                                                                                               option_chain=option_chain)
            df = pd.read_sql(query, con=conn)
        return df

    @staticmethod
    def _get_result(option_chain=None, stock_list=None):
        puts_dfs = []
        calls_dfs = []
        if option_chain is None:
            option_chain = InvestmentUtil.get_next_friday_str()
        stock_list = stock_list if stock_list is not None else WheelPickem.DEFAULT_STOCK_LIST
        logging.info("Finding pick for option-chain: " + option_chain)
        for stock in stock_list:
            res = OptionsFetcher.fetch_covered_wheel_data(stock, option_chain)
            if res is not None:
                puts, calls = res
                puts_dfs.append(puts)
                calls_dfs.append(calls)
        return option_chain, pd.concat(puts_dfs), pd.concat(calls_dfs)

    @staticmethod
    def main(option_chain=None, stock_list=None):
        logging.basicConfig(level=logging.INFO)
        logging.info("Starting pick-em script")
        option_chain, puts_df, calls_df = WheelPickem._get_result(option_chain=option_chain, stock_list=stock_list)
        generator = HtmlGenerator(Reports.PICKEM_REPORT)
        content_kwargs = {
            'week_name': option_chain,
            'puts_table': puts_df.to_html(),
            'calls_table': calls_df.to_html()
        }
        generator.generate(content_kwargs, output_location=WheelPickem.OUTPUT_FILE)
        logging.info("Sending results email")
        InvestmentUtil.send_html_as_gmail(WheelPickem.MESSAGE,
                                        'Covered Puts Picks for week of {}'.format(option_chain),
                                          WheelPickem.RECIPIENTS, WheelPickem.OUTPUT_FILE)

    @staticmethod
    def update_database():
        logging.basicConfig(level=logging.INFO)
        logging.info("Updating pick-em database")
        net_puts = []
        net_calls = []
        start_date = InvestmentUtil.get_next_friday_dt()
        for i in range(WheelPickem.NUMBER_OF_WEEKS_TO_GATHER):
            start_date_str = InvestmentUtil.format_to_date_string(start_date)
            logging.info("Gathering info for week of: {}".format(start_date_str))
            try:
                option_chain, puts, calls = WheelPickem.get_as_df(start_date_str, WheelPickem.DEFAULT_STOCK_LIST)
            except ValueError:
                logging.info("No chain for stock and week selected")
                continue
            net_puts.append(puts)
            net_calls.append(calls)
            start_date = start_date + relativedelta.relativedelta(weeks=1)


        engine = InvestmentUtil.get_database_engine()
        with engine.begin() as connection:
            connection.execute("DROP TABLE IF EXISTS puts")
            connection.execute("DROP TABLE IF EXISTS calls")
            logging.info("Writing to db")
            puts = pd.concat(net_puts)
            calls = pd.concat(net_calls)
            puts.to_sql("puts", con=connection, if_exists='append')
            calls.to_sql("calls", con=connection, if_exists='append')
            logging.info("Write complete")


#
#     roi + investment
#     % OTM/ATM
#     technical details
#         RSI > 70 overbought -> going down
#         RSI < 30 oversold -> going up
#         - RSI10 and RSI14 daily (close?)
#         MACD
#             default of 12 (short term), 26 (long), 9 (signal)
#             look for MACD crossing the signal line = bearish (down)
#             shoot up singal ( = go up)
#             MACD towards 0 axis sometimes bounce
#
#
# ballenger
# band
#             2 STD out from moving average
#             most trading in those bounds
#             scraping the bottom edge = bounce soon
# 50 day / 200 day
#


if __name__ == '__main__':
    WheelPickem.update_database()
