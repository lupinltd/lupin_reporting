import pandas as pd

class TechnicalAnalysis():

    RSI_BEARISH = 70
    RSI_BULLISH = 30

    @staticmethod
    def get_rsi(price_series, window):
        delta = price_series.diff()
        up_days = delta.copy()
        up_days[delta <= 0] = 0.0
        down_days = abs(delta.copy())
        down_days[delta > 0] = 0.0
        RS_up = up_days.rolling(window).mean()
        RS_down = down_days.rolling(window).mean()
        rsi = 100 - 100 / (1 + RS_up / RS_down)
        rsi.name = 'rsi'
        rsi_df = rsi.to_frame()
        rsi_df['rsi_indicator'] = 'UNDETERMINED'
        rsi_df.loc[rsi_df['rsi'] >= TechnicalAnalysis.RSI_BEARISH, 'rsi_indicator'] = 'BEARISH'
        rsi_df.loc[rsi_df['rsi'] <= TechnicalAnalysis.RSI_BULLISH, 'rsi_indicator'] = 'BULLISH'
        return rsi_df