import os


GMAIL_SERVER = "smtp.gmail.com"
SENDER_EMAIL = os.environ.get("LUPIN_SEND_EMAIL")
SENDER_PASSWORD = os.environ.get("LUPIN_SEND_PASSWORD")
DATABASE_URL = 'DATABASE_URL'
if not DATABASE_URL in os.environ:
    raise Exception("DATABASE_URL has not been set in the env variables.  Please set")
SQLALCHEMY_DATABASE_URI = os.environ[DATABASE_URL]
