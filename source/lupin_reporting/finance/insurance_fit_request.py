from dataclasses import dataclass
from lupin_reporting.finance.insurance_fit import InsuranceFit


@dataclass
class InsuranceFitRequest:
    t_10: bool = True
    t_20: bool = True
    t_30: bool = True
    t_100: bool = True
    fit_strategy: InsuranceFit = InsuranceFit.AVERAGE

    @property
    def no_fit_set(self):
        not insurance_fit_request.t_10 and not insurance_fit_request.t_20 \
        and not insurance_fit_request.t_30 and not insurance_fit_request.t_100

    @property
    def t10_start_index(self):
        return 0

    def t10_end_index(self):
        if self.t_10:
            return 10
        elif self.t_20:
            return 20
        elif self.t_30:
            return 30
        elif self.t_100:
            return -1

    @property
    def t20_start_index(self):
        if self.t_10:
            return 10
        else:
            return 0

    @property
    def t20_end_index(self):
        if self.t_20:
            return 20
        elif self.t_30:
            return 30
        elif self.t_100:
            return -1

    @property
    def t30_start_index(self):
        if self.t_10:
            if self.t_20:
                return 20
            else:
                return 10
                pass
        elif self.t_20:
            pass
        else:
            return 0

    @property
    def t30_end_index(self):
        if self.t_20:
            return 20
        elif self.t_30:
            return 30
        elif self.t_100:
            return -1
