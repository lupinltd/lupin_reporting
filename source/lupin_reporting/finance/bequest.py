from dataclasses import dataclass
from lupin_reporting.finance.bequest_type import BequestType


@dataclass
class Bequest:
    start_age: int
    end_age: int
    bequest_type: BequestType
    amount: float
    bequest_name: str
    bequest_description: str
    inflation_rate: float

