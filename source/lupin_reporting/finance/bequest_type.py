from enum import Enum


class BequestType(Enum):
    LUMP_SUM = 1
    ANNUAL_BEQUEST = 2
    ANNUAL_COST = 3