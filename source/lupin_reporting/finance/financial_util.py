import statistics
from lupin_reporting.finance.bequest_type import BequestType
from lupin_reporting.finance.bequest import Bequest
from lupin_reporting.finance.insurance_fit_request import InsuranceFitRequest
from lupin_reporting.finance.insurance_fit import InsuranceFit


class FinancialUtil:

    def __init__(self):
        pass

    def future_value(self, pv, rate, compound_periods):
        return pv * (1 + rate) ** compound_periods

    def present_value(self, fv, rate, compound_periods):
        return fv / ((1+rate)**compound_periods)

    def compute_income_replacement_array(self, age, current_salary, retirement_age, mortality_age, income_needed_array,
                                  salary_growth_rate, growth_rate):
        net_salary_needed = self.compute_net_salary_needed(age, current_salary, retirement_age, mortality_age,
                                                           income_needed_array, salary_growth_rate)
        income_replacement_array = []
        for age_index, salary_needed in enumerate(net_salary_needed):
            pv_cumsum = 0
            for j in range(age_index + 1, (mortality_age - age)):
                pv_cumsum += self.present_value(net_salary_needed[j], growth_rate, (j-age_index))
            income_replacement = salary_needed + pv_cumsum
            income_replacement_array.append(income_replacement)
        return income_replacement_array

    def compute_net_salary_needed(self, age, current_salary, retirement_age, mortality_age, income_needed_array,
                                  salary_growth_rate):
        net_salary_array = []
        year_index = 0
        while age < mortality_age:
            salary_needed = 0
            if len(net_salary_array) == 0:
                salary_needed = current_salary * (1 + salary_growth_rate) * income_needed_array[year_index]
            elif age <= retirement_age:
                salary_needed = net_salary_array[year_index - 1] * (1 + salary_growth_rate)
            net_salary_array.append(salary_needed)
            year_index += 1
            age += 1
        return net_salary_array

    def compute_lump_sum_bequest(self, age, bequest: Bequest):
        bequests = []
        current_age = age
        while current_age < bequest.end_age:
            if current_age > bequest.start_age and current_age < bequest.end_age:
                bequests.append(self.future_value(bequest.amount, bequest.inflation_rate, (current_age - age)))
            else:
                bequests.append(0)
            current_age += 1
        return bequests

    def compute_bequest(self, age, bequest):
        if bequest.bequest_type == BequestType.LUMP_SUM:
            return self.compute_lump_sum_bequest(age, bequest)
        else:
            raise NotImplementedError("bequest type not yet implemented")

    def perform_insurance_fit(self, insurance_need_array, insurance_fit_request: InsuranceFitRequest):
        # TODO: clean this up, this is gross
        fit_values = []
        no_fit_set = insurance_fit_request.no_fit_set
        for j, insurance_need in enumerate(insurance_need_array):
            if j < 10:
                start_index = insurance_fit_request.t_10_start_index
                end_index = insurance_fit_request.t_10_end_index
                val = self.insurance_fit_list(insurance_need_array[start_index:end_index],
                                              insurance_fit_request.fit_strategy) if not no_fit_set else 0
            elif j < 20:
                start_index = insurance_fit_request.t_20_start_index
                end_index = insurance_fit_request.t_20_end_index
                val = self.insurance_fit_list(insurance_need_array[start_index:end_index],
                                              insurance_fit_request.fit_strategy) if not no_fit_set else 0
            elif j < 30:
                """
                =IF(
                    M34=1,
                    IF(M35=1,
                        IF(M36=1,
                            AVERAGE(C29:C39),
                            IF(M37=1,
                                AVERAGE(C29:C71),
                                0)
                        ),
                        IF(
                            M36=1,
                            AVERAGE(C19:C39),IF(M37=1,AVERAGE(C19:C71),0))),IF(M35=1,IF(M36=1,AVERAGE(C29:C39),IF(M37=1,AVERAGE(C29:C71),0)),IF(M36=1,AVERAGE(C9:C39),IF(M37=1,AVERAGE(C9:C71),0))))
                """
                pass
            else:
                pass
            fit_values.append(val)
        return fit_values

    def insurance_fit_list(self, need_array, fit_algorithm: InsuranceFit):
        if fit_algorithm == InsuranceFit.AVERAGE:
            return self._perform_average_fit(need_array)
        else:
            raise NotImplementedError

    def _perform_average_fit(self, need_array):
        return statistics.mean(need_array)
