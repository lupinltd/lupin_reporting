from lupin_reporting.plotting.plotter import Plotter
from plotly.graph_objs import Data, Figure, Scatter


class PlotlyPlotter(Plotter):

    DEFAULT_COLOR = '#17BECF'
    DEFAULT_OPACITY = 0.8

    def __init__(self):
        pass

    def generate_figure(self, **kwargs):
        raise NotImplementedError

    def to_html(self, figure, **kwargs):
        #TODO allow input settings
        return figure.to_html(full_html=False, default_height="800px", default_width="60%", include_plotlyjs=False)

    def make_plot_scatter_series(self, x, y, name, color=DEFAULT_COLOR, opacity=DEFAULT_OPACITY):
        trace1 = {
            "line": {"color": color},
            "name": name,
            "type": "scatter",
            "x": x,
            "y": y,
            "opacity": opacity
        }
        return Scatter(trace1)

    def make_figure(self, data, title):
        layout = {
            "title": title
        }
        return Figure(data=data, layout=layout)
