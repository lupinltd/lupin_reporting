import abc


class Plotter(abc.ABC):

    @classmethod
    def generate_figure(self, **kwargs):
        pass

    @classmethod
    def as_html(self, **kwargs):
        pass

    @classmethod
    def to_html(self, **kwargs):
        pass
