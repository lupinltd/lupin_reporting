from plotly.graph_objs import Data, Figure
from lupin_reporting.plotting.plotly_plotter import PlotlyPlotter
from lupin_reporting.finance.financial_util import FinancialUtil


class NeedAnalysisPlotter(PlotlyPlotter):
    DEFAULT_INFLATION_RATE = 0.0220
    DEFAULT_GROWTH_RATE = 0.0350
    DEFAULT_SALARY_GROWTH_RATE = 0.0300
    DEFAULT_FIGURE_NAME = 'Insurance Need Over Time'

    def __init__(self):
        super(NeedAnalysisPlotter, self).__init__()
        self._financial_util = FinancialUtil()

    def _generate_time_axis(self, age, mortality_age):
        return [j for j in range(age, mortality_age)]

    def generate_figure(self, age, mortality_age, percent_income_needed, current_salary, retirement_age, bequests=None,
                        inflation_rate=DEFAULT_INFLATION_RATE, growth_rate=DEFAULT_GROWTH_RATE,
                        salary_growth_rate=DEFAULT_SALARY_GROWTH_RATE, figure_name=DEFAULT_FIGURE_NAME):
        income_replacement = self._financial_util.compute_income_replacement_array(age, current_salary, retirement_age,
                                                                                   mortality_age, percent_income_needed,
                                                                                   salary_growth_rate, growth_rate)
        net_life_insurance_need = income_replacement
        if bequests is not None:
            raise NotImplementedError
        plot = self.make_figure(self.make_plot_scatter_series(self._generate_time_axis(age, mortality_age),
                                                              net_life_insurance_need, figure_name), figure_name)
        return plot

    def as_html(self, age, mortality_age, percent_income_needed, current_salary, retirement_age, bequests=None,
                        inflation_rate=DEFAULT_INFLATION_RATE, growth_rate=DEFAULT_GROWTH_RATE,
                        salary_growth_rate=DEFAULT_SALARY_GROWTH_RATE):
        figure = self.generate_figure(age, mortality_age, percent_income_needed, current_salary, retirement_age,
                                      bequests, inflation_rate, growth_rate, salary_growth_rate)
        return self.to_html(figure)

    def _kwargs_to_default_arguments(self, **kwargs):
        pass