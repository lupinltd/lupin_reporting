__author__ = 'Matt Cordoba'
from setuptools import setup,find_packages
try: # for pip >= 10
    from pip._internal.req import parse_requirements
except ImportError: # for pip <= 9.0.3
    from pip.req import parse_requirements
install_reqs = parse_requirements('requirements.txt',session=False)
try:
    reqs = [str(ir.req) for ir in install_reqs]
except:
    reqs = [str(ir.requirement) for ir in install_reqs]

setup(name='lupin_reporting',
      version='1.0',
      description='Reporting Stack for Lupin Products',
      classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.7.1',
        'Topic :: Text Processing :: Linguistic',
      ],
      author='Matt Cordoba',
      author_email='cordobamatt@gmail.com',
      package_dir={'': 'source'},
      packages=find_packages(where='source'),
      data_files=['source/lupin_reporting/html/templates/insurance_report.html'],
      package_data= {
        'lupin_reporting': ['source/lupin_reporting/html/templates/*.html']
      },
      install_requires=reqs,
      include_package_data=True,
      zip_safe=False)